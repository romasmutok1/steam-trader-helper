import Utils from './Utils.js';
import {WSClient, WSMsg, WSMsgType} from './websocket.js';

const utils = new Utils();

export default function TraderAPI () {}

TraderAPI.prototype.isAuthedWithSteamid = async function(steamid) {
	let response = await fetch('https://steam-trader.com/api/getsteamid');

	if(response.ok === false) {
		return false;
	}
	let json = await response.json(); 

	return json.success !== undefined && json.data == steamid;
};

TraderAPI.prototype.isSteamApiKeySaved = async function(key) {
	let response = await fetch('https://steam-trader.com/api/getsteamapikey');
	if(response.ok === false) {
		return false;
	}
	let json = await response.json(); 

	return json.success !== undefined && json.data == key;
};

TraderAPI.prototype.saveSteamApiKeyOnTraderAccount = async function(key) {
	let url = 'https://steam-trader.com/settings/save/';
	let fd = new FormData();
	fd.set('steamApiKey', key);
	
	let cookie = await utils.getCookie('csrf_token');
	fd.set('csrf_token', cookie.value);
	
	let params = utils.urlencodeFormData(fd);

	let response = await fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		},
		body: params
	});

	let json = await response.json();

	return json.success;
};

TraderAPI.prototype.InitWSIfNotAndAddTradeMsgListener = async function(callback) {
	let self = this;

	if(WSClient.connected === false)
		initWS(callback);
	else
		await trySendWSAuthMsg();

	function initWS(callback) {
			WSClient.DeleteAllHandlers();

			WSClient.AddHandler(WSMsgType.Auth, async function(msg) {
				console.log('ws on auth msg', msg);
				if(msg == "0")
					await trySendWSAuthMsg();
			}, false);

			WSClient.AddHandler(WSMsgType.SendP2pOffer, callback);

			WSClient.Connect(async function() {
				console.log('ws on connected');
				await trySendWSAuthMsg();
			});
	}

	async function trySendWSAuthMsg() {
		let wsToken = null;
		try
		{
			wsToken = await getWSToken();
			if(!wsToken)
				return false;
			let message = new WSMsg(WSMsgType.Auth, wsToken);
			WSClient.Send(message);
		}
		catch(e)
		{
			console.log('e',e);
		}
	}

	async function getWSToken() {
		let response = await fetch('https://steam-trader.com/api/getwstoken');

		if(response.ok === false) {
			return false;
		}

		return await response.json();
	};
};