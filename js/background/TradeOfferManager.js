import Utils from './Utils.js';

const utils = new Utils();

export default function TradeOfferManager (steam, trader, health) {
	this.currentPartnerTradeUrl = null;
	this.steam = steam;
	this.trader = trader;
	this.health = health;
	this.listeningForUpdateOffers = {};
	this.listeningOffersUpdatesTimer = null;
}

TradeOfferManager.prototype.checkAllHealthAndReload = async function() {
	await this.health.check();

	return await reload(this);
}

TradeOfferManager.prototype.checkSteamHealthAndReload = async function() {
	await this.health.checkSteam();
	
	return await reload(this);
}

TradeOfferManager.prototype.checkTraderHealthAndReload = async function() {
	await this.health.checkTrader();
	
	return await reload(this);
}

async function reload(self) {

	//if(self.health.allIsOk()) {
        self.onTradeMessageFromTrader(async (msg) => {
	    	if(!self.health.allIsOk()) {
	            await utils.sendAlertToUserUi("Торговля CS:GO на вашем аккаунте не работает - нажмите на иконку расширения, чтобы проверить проблемы!");
	            return false;
	        }
			if(await self.isOfferAlreadyCreatedInSteam(msg)) {
	            await utils.sendAlertToUserUi("Предложение обмена уже создано. Проверьте стим.");
	            return false;
	        }
			let createRes = await self.createOfferInSteam(msg);
	        if(createRes === null) {
	            let strError = "При отправке этого предложения обмена произошла ошибка. Пожалуйста, повторите попытку."
	            await utils.sendAlertToUserUi(strError);
	            await self.health.check();
	        } else if(createRes.strError != undefined) {
	            let strError = createRes.strError;
	            await utils.sendAlertToUserUi(strError);
	            await self.health.check();
	        } else {
	            let sendRes = await self.sendOfferCreatingResultToTrader(createRes);
	            self.listenOfferStatusChangesAndSendItToTrader(createRes);
	        }
		});
    //}

    return self.health.getInfo();
}

TradeOfferManager.prototype.onTradeMessageFromTrader = function(callback) {

	let self = this;

	self.trader.InitWSIfNotAndAddTradeMsgListener(function(msg) {
		self.currentPartnerTradeUrl = msg.partnerTradeUrl;
		callback(msg);
	});
};

/**
 * Пытается создать оффер в стиме по апи
 * @param  {object} msg данные обмена
 * @return {object|null} ответ в json или null, если http status != 200
 */
TradeOfferManager.prototype.createOfferInSteam = async function(msg) {

	return await this.steam.createTradeOffer(msg);
};

TradeOfferManager.prototype.sendOfferCreatingResultToTrader = async function(result) {

	if(result.tradeofferid === undefined)
		return false;

	let url = 'https://steam-trader.com/api/OnP2POfferCreated/?assetId='+result.assetid+'&offerId='+result.tradeofferid+'&offerMsg='+result.message;
	let response = await fetch(url);

	return await response.json();
};

TradeOfferManager.prototype.sendOfferSendingResultToTrader = async function(result) {
	if(result.tradeofferid === undefined)
		return false;

	let url = 'https://steam-trader.com/api/OnP2POfferSent/?assetId='+result.assetid+'&offerId='+result.tradeofferid+'&offerMsg='+result.message;
	let response = await fetch(url);

	return await response.json();
};

TradeOfferManager.prototype.isOfferAlreadyCreatedInSteam = async function(data) {
	let result = false;

	let key = this.steam.accountApiKey;
	let url = 'https://api.steampowered.com/IEconService/GetTradeOffers/v1/?key='+key+'&get_sent_offers=1&active_only=1';
	let response = await fetch(url);
	let json = await response.json();

	for(let i in json.response.trade_offers_sent) {
		if(result === true)
			break;
		let offer = json.response.trade_offers_sent[i];
		for(let j in offer.items_to_give) {
			let item = offer.items_to_give[j];
			if(
				item.assetid === data.assetid
			//https://developer.valvesoftware.com/wiki/Steam_Web_API/IEconService#ETradeOfferState
			&&  ![5,6,7,10].includes(offer.trade_offer_state)
			)
			{
				result = true;
				break;
			}
		}
	}

	return result;
};

TradeOfferManager.prototype.listenOfferStatusChangesAndSendItToTrader = function(offer) {
	let self = this;
	offer.isBuyerNotified = false;
	self.listeningForUpdateOffers[offer.tradeofferid] = offer;
	if(self.listeningOffersUpdatesTimer == null) {
		self.listeningOffersUpdatesTimer = setInterval(async function() {
			if(Object.keys(self.listeningForUpdateOffers).length === 0) {
				clearInterval(self.listeningOffersUpdatesTimer);
				self.listeningOffersUpdatesTimer = null;
			}
			
			for(let tradeofferid in self.listeningForUpdateOffers) {
				let traderOfferData = self.listeningForUpdateOffers[tradeofferid];
				let steamOfferData = await self.steam.getTradeOffer(tradeofferid);
				let steamOffer = steamOfferData.response.offer;

				if(steamOffer.trade_offer_state === 2
				&& !traderOfferData.isBuyerNotified
				) {
					let res = await self.sendOfferSendingResultToTrader(traderOfferData);
					if(res.success === true)
						traderOfferData.isBuyerNotified = true;
				}

				if(![2,9].includes(steamOffer.trade_offer_state)) {
					delete self.listeningForUpdateOffers[tradeofferid];
					await sendToTraderThatOfferChangedStatus(traderOfferData);
				}
			}
		}, 10000);
	}
};

async function sendToTraderThatOfferChangedStatus(data) {
	if(data.tradeofferid === undefined)
		return false;

	let url = 'https://steam-trader.com/api/OnP2POfferChangedStatus/?assetId='+data.assetid+'&offerId='+data.tradeofferid;
	let response = await fetch(url);

	return await response.json();
}